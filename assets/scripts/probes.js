var Probes = {
	hasLocal: false,
	current: {},
	init: function () {
		var _ = this;
		if (typeof localStorage == "object")
			_.hasLocal = true;

		var hash = location.hash.substr(1);
		var local = _.getLocal();

		if (hash) {
			Share.setData(hash);
		} else if (local) {
			for (var site in local) {
				data.sites[site].current = local[site];
				_.setProbe(site, data.sites[site].current.id);
			}
		} else {
			for (var site in data.sites)
				_.setProbe(site, 'basic');
		}
		_.bind();
		_.updateLinked();
	},
	bind: function () {
		var list = QSA('li[data-site]');
		for (var i = 0; i < list.length; i++) {
			if (list[i].getAttribute('data-site') == "NLA")
				continue;
			list[i].addEventListener('click', function (e) {
				e.stopPropagation();
			});
			list[i].addEventListener('focus', function (e) {
				e.stopPropagation();
				for (var j = 0; j < list.length; j++)
					list[j].className = list[j].className.replace(/\s*opened/, '');
				this.className += ' opened';
				InfoPanel.buildSpotList(this.getAttribute('data-site'));
				InfoPanel.displayProbeData(this.getAttribute('data-site'));
			});
		}

		QS('.map').addEventListener('click', function (e) {
			e.stopPropagation();
			Probes.unselect();
		});
	},
	unselect: function (site) {
		var list = QSA('li[data-site]');
		for (var j = 0; j < list.length; j++)
			list[j].className = list[j].className.replace(/\s*opened/, '').replace(/^\s*/, '');
		InfoPanel.closeProbeData();
	},
	getProbe: function (site) {
		if (typeof data.sites[site] == "undefined")
			return false;

		return data.sites[site];
	},
	setProbe: function (site, type) {
		var _ = this;
		var elem = QS('li[data-site="' + site + '"]');
		if (!elem || typeof data.sites[site] == "undefined" || typeof data.probe[type] == "undefined")
			return false;
		_.current[site] = JSON.parse(JSON.stringify(data.probe[type])); // Clone object
		_.current[site].id = type;
		if (typeof data.sites[site].current != "undefined" && typeof data.sites[site].current.spot != "undefined")
			_.current[site].spot = JSON.parse(JSON.stringify(data.sites[site].current.spot)); // Clone object
		data.sites[site].current = JSON.parse(JSON.stringify(_.current[site])); // Clone object

		if (site == 'NLA') {
			elem.className = "nla";
		} else {
			elem.className = elem.className.replace(/basic|storage|production|revenue|booster|duplicator|fuel|melee|ranged|debuff|resistance/, '');
			elem.className += ' ' + data.probe[type].type;
			elem.className = elem.className.replace(/^\s/, '');
			elem.className = elem.className.replace(/\s{2,}/, ' ');
			var level = /[\d]+/.exec(type);
			if (level)
				elem.setAttribute('data-level', level[0]);
			else
				elem.removeAttribute('data-level');
		}
	},
	setProbeSpot: function (site, spot) {
		var _ = this;
		_.current[site].spot = spot;
		data.sites[site].current = JSON.parse(JSON.stringify(_.current[site])); // Clone object
	},
	getLocal: function () {
		var _ = this;
		if (!_.hasLocal)
			return false;
		var temp;
		if (!(temp = localStorage.getItem("probesData")))
			return false;
		if (!(temp = JSON.parse(temp)))
			return false;
		return temp;
	},
	setLocal: function () {
		var _ = this;
		if (!_.hasLocal)
			return false;
		localStorage.setItem("probesData", JSON.stringify(_.current));
	},
	updateLinked: function () {
		for (var y in data.sites) {
			if (y != 'NLA') {
				data.sites[y].current.linked = Probes.countLinkedSameTypeId(y, data.sites[y].current.id);
				data.sites[y].current.boosted = Probes.countLinkedBoosters(y);
			}
		}
		for (var i in data.sites) {
			if (i == 'NLA')
				continue;

			var site = QS('li[data-site="' + i + '"]');
			site.classList.remove('linked');
			var linked = data.sites[i].current.linked;
			var boosted = data.sites[i].current.boosted;
			// console.log(i, data.sites[i].current.boosted);

			var cleanLinks = QSA('svg[data-link*="' + i + '"]');
			for (var l = 0; l < cleanLinks.length; l++) {
				cleanLinks[l].classList.remove('linked');
				cleanLinks[l].classList.remove('boosted');
			}

			for (var j = 0; j < boosted.length; j++) {
				var link = QS('svg[data-link*="' + i + '"][data-link*="' + boosted[j] + '"]');
				if (link && i != boosted[j])
					link.classList.add('boosted');
			}

			if (data.sites[i].current.id == "basic")
				continue;

			for (var j = 0; j < linked.length; j++) {
				var site = QS('li[data-site="' + i + '"]');
				site.classList.remove('linked');

				if (linked.length <= 2)
					continue;

				site.classList.add('linked');

				var link = QS('svg[data-link*="' + i + '"][data-link*="' + linked[j] + '"]');
				if (link && i != linked[j])
					link.classList.add('linked');
			}
		}
	},
	countLinkedSameTypeId: function (site, typeId, parent, linked) {
		site = parseInt(site);
		var probe = data.sites[site];
		if (!parent || typeof parent != 'object')
			var parent = [];
		parent.push(site);
		if (!linked || typeof linked != 'object')
			var linked = [];
		linked.push(site);

		for (var i = 0; i < probe.link.length; i++) {
			var probeData = Probes.getProbe(probe.link[i]);
			if (probe.link[i] != 'NLA' && typeof probeData.current == 'object' && probeData.current.id == typeId && !parent.inArray(probe.link[i]))
				linked.concat(Probes.countLinkedSameTypeId(probe.link[i], typeId, parent, linked));
		}
		return linked;
	},
	countLinkedBoosters: function (site) {
		site = parseInt(site);
		var probe = data.sites[site];
		if (!linked)
			var linked = [];

		var probeData = Probes.getProbe(probe);
		if (probe.current.type == 'booster' || probe.current.type == 'storage' || probe.current.type == 'duplicator') {
			for (var i = 0; i < probe.link.length; i++) {
				if (probe.link[i] != 'NLA')
					linked.push(probe.link[i]);
			}
		}
		return linked;
	},
	getLinkedBooster: function (site) {
		site = parseInt(site);
		var probe = data.sites[site];
		var boost = [];

		for (var i = 0; i < probe.link.length; i++) {
			var probeData = Probes.getProbe(probe.link[i]);
			if (probe.link[i] != 'NLA' && typeof probeData.current == 'object' && probeData.current.type == 'booster')
				boost.push(data.probe[probeData.current.id].values.boost / 100 + 1);
			else if (probe.link[i] != 'NLA' && typeof probeData.current == 'object' && probeData.current.type == 'duplicator')
				boost = boost.concat(this.getLinkedBooster(probe.link[i]));
		}
		return boost;
	},
	getLinkedSites: function (site) {
		site = parseInt(site);
		var probe = data.sites[site];
		var probesList = [];

		for (var i = 0; i < probe.link.length; i++) {
			var probeData = Probes.getProbe(probe.link[i]);
			if (probe.link[i] != 'NLA' && typeof probeData.current == 'object' && probeData.current.type != 'duplicator')
				probesList.push(probe.link[i]);
		}
		return probesList;
	},
	getBonus: function (data) {
		var _ = this;
		if (data.current.type == 'production' || data.current.type == 'revenue' || data.current.type == 'storage') {
			var l = data.current.linked.length;
			if (l >= 8)
				return 1.8;
			else if (l >= 5)
				return 1.5;
			else if (l >= 3)
				return 1.3;
			else
				return 1;
		} else {
			return 1;
		}
	}
};
