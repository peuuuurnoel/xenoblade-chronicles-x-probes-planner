var Ressources = {
	fnTotal: null,
	total: null,
	init: function () {
		var _ = this;
		_.fnTotal = QS('#fn-total');
		_.update();
	},
	calculate: function () {
		var _ = this;
		_.total = {
			storage: 6000,
			production: 0,
			revenue: 0
		};
		var tempProbesData = {};
		var duplicatorProbesList = [];
		var count = {};
		for (var site in data.sites) {
			var probe = data.sites[site];

			if (site == 'NLA')
				continue;

			if (probe.current.id != 'basic') {
				if (typeof count[probe.current.id] != 'number')
					count[probe.current.id] = 0;
				count[probe.current.id]++;
			}

			var stats = {};

			if (probe.current.type == 'duplicator') {
				var linkedSites = Probes.getLinkedSites(site);
				var linkedSite = null;
				var tempStats = {
					storage: 0,
					production: 0,
					revenue: 0
				};
				for (var l = 0; l < linkedSites.length; l++) {
					linkedSite = linkedSites[l];
					var temp = _.getProbeStats(Probes.getProbe(linkedSite), site, probe);

					tempStats.production += temp.production;
					tempStats.revenue += temp.revenue;
					tempStats.storage += temp.storage;
				}
				stats = tempStats;
			} else {
				stats = _.getProbeStats(probe, site, false);
			}

			_.total.production += stats.production;
			_.total.revenue += stats.revenue;
			_.total.storage += stats.storage;
		}

		if (!Main.IEfix) {
			for (var c in InfoPanel.probeCount) {
				if (count[c])
					InfoPanel.probeCount[c].innerHTML = count[c].toString().padLeft('&nbsp;', 2);
				else
					InfoPanel.probeCount[c].innerHTML = '&nbsp;' + 0;
			}
		}
	},
	getProbeStats: function (probe, site, duplicator) {
		var _ = this;
		var rank = data.fn;
		var stats = {
			storage: 0,
			production: 0,
			revenue: 0
		};

		probe = JSON.parse(JSON.stringify(probe)); // Clone object

		if (duplicator) {
			duplicator = JSON.parse(JSON.stringify(duplicator)); // Clone object

			probe.rankName.production = duplicator.rankName.production;
			probe.rankName.revenue = duplicator.rankName.revenue;
		}

		if (probe.current.type != 'duplicator') {
			var production = probe.current.values.production * rank.production[probe.rankName.production];
			var revenue = 0;
			if (data.sites[site].rankValue && data.sites[site].rankValue.revenue)
				revenue = probe.current.values.revenue * data.sites[site].rankValue.revenue;
			else if (probe.rankName.revenue == 'A')
				revenue = probe.current.values.revenue * rank.revenue[probe.rankName.revenue][probe.area];
			else
				revenue = probe.current.values.revenue * rank.revenue[probe.rankName.revenue];

			var bonus = Probes.getBonus(probe);
			if (probe.current.type == 'production') {
				if (!duplicator) {
					production *= bonus;

					// Fix for "Mining Probe G5" 30% bonus link.
					// Remove 1 point for each probes under this bonus...
					// Works well for all other mining probes and bonus level.
					// So 3 or 4 points are subtracted.
					if (probe.current.id == 'pg5' && (probe.current.linked.length == 3 || probe.current.linked.length == 4))
						production--;
				}
				var boosterList = Probes.getLinkedBooster(site);
				for (var i = 0; i < boosterList.length; i++)
					production *= boosterList[i];
			}
			if (probe.current.type == 'revenue') {
				if (duplicator !== false && data.sites[site].current.spot && data.sites[site].current.spot > 0)
					revenue += probe.current.values.revenue * 10 * data.sites[site].current.spot;
				else if (duplicator === false && probe.current.spot && probe.current.spot > 0)
					revenue += probe.current.values.revenue * 10 * probe.current.spot;
				if (!duplicator && bonus > 1)
					revenue *= bonus;
				var boosterList = Probes.getLinkedBooster(site);
				for (var i = 0; i < boosterList.length; i++)
					revenue *= boosterList[i];
			}

			stats.production += production;
			stats.revenue += revenue;

			if (probe.current.type == 'storage') {
				var storage = probe.current.values.storage;
				if (!duplicator)
					storage *= bonus;
				var boosterList = Probes.getLinkedBooster(site);
				for (var i = 0; i < boosterList.length; i++)
					storage *= boosterList[i];
				stats.storage += storage;
			}
		}
		return stats;
	},
	display: function () {
		var _ = this;
		QS('.fn-total-production', _.fnTotal).innerText = Math.floor(_.total.production);
		QS('.fn-total-revenue', _.fnTotal).innerText = Math.floor(_.total.revenue);
		QS('.fn-total-storage', _.fnTotal).innerText = Math.floor(_.total.storage);
	},
	update: function () {
		var _ = this;
		_.calculate();
		_.display();
	}
};
