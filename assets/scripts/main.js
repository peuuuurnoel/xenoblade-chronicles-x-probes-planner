var Main = {
	IEfix: false,
	init: function () {
		if (/Trident|MSIE|Edge/i.test(navigator.userAgent))
			this.IEfix = true;
		WorldMap.init();
		InfoPanel.init();
		Probes.init();
		Ressources.init();
	}
};
