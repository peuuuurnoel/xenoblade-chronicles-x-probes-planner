var Share = {
	getLink: function () {
		var temp = '';
		for (var i in Probes.current) {
			if (i == 'NLA')
				continue;
			var t = [];
			var probe = Probes.current[i];
			if (probe.id != 'basic')
				t.push(probe.id);
			if (probe.spot > 0)
				t.push(probe.spot);
			temp += t.join('-') + '|';
		}
		temp = temp.replace(/\|$/, '');
		location.hash = LZString.compressToBase64(JSON.stringify(temp));
	},
	setData: function (string) {
		Probes.unselect();
		string = string.replace(/\s|%20/g, '+');
		var temp = LZString.decompressFromBase64(string).replace(/"/g, '').split('|');
		var j = 0;
		for (var i in data.sites) {
			if (i == 'NLA')
				continue;
			var t = temp[j].split('-');
			var current = {
				id: 'basic',
				spot: 0
			};
			j++;

			if (t.length == 1 && t[0] != '' && t[0] >>> 0 == 0) {
				current.id = t[0];
			} else if (t.length == 1 && t[0] != '' && t[0] >>> 0 > 0) {
				current.spot = t[0];
			} else if (t.length == 2) {
				current.id = t[0];
				current.spot = t[1];
			}

			Probes.setProbe(i, current.id);
			Probes.setProbeSpot(i, current.spot);
		}
		document.querySelector('li[data-site="NLA"]').className = 'nla';

		Probes.updateLinked();
		Probes.setLocal();
		Ressources.init();
	}
};
