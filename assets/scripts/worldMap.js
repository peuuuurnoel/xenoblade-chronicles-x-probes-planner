var WorldMap = {
	isDragging: false,
	map: null,
	mapPos: {
		x: 0,
		y: 0
	},
	windowSize: {
		height: 0,
		width: 0
	},
	imageSize: {
		height: 0,
		width: 0
	},
	currentScale: 1,
	probes: [],
	init: function () {
		var _ = this;
		_.map = QS("#worldmap .map");

		_.getWindowSize();
		_.resize();
		_.bindMove();
		_.zoom();
		_.placeProbeSite();
		_.addLinks();
	},
	getWindowSize: function () {
		var _ = this;
		_.windowSize.height = window.innerHeight;
		_.windowSize.width = window.innerWidth;

		var worldmapImage = QS('img', _.map);
		_.imageSize.height = worldmapImage.offsetHeight;
		_.imageSize.width = worldmapImage.offsetWidth;

		_.map.style.transform = 'translate(0, 0) scale(' + _.currentScale + ')';
	},
	resize: function () {
		window.addEventListener('resize', function () {
			WorldMap.getWindowSize();
		});
	},
	bindMove: function () {
		var _ = this,
		posStart = {
			x: 0,
			y: 0
		},
		posCurrent = {
			x: 0,
			y: 0
		};
		_.map.addEventListener('mousedown', function (event) {
			_.isDragging = true;
			posStart.x = event.clientX;
			posStart.y = event.clientY;
			posCurrent.x = event.clientX;
			posCurrent.y = event.clientY;
			this.classList.add('isGrabbing');
		});
		_.map.addEventListener('mousemove', function (event) {
			if (!_.isDragging)
				return true;
			_.calculatePos(event, posCurrent);

			posCurrent.x = event.clientX;
			posCurrent.y = event.clientY;
		});
		_.map.addEventListener('mouseup', function (event) {
			_.isDragging = false;
			_.calculatePos(event, posCurrent);
			this.classList.remove('isGrabbing');
		});
		_.map.addEventListener('mouseleave', function (event) {
			if (!_.isDragging)
				return true;
			_.isDragging = false;
			_.calculatePos(event, posCurrent);
			this.classList.remove('isGrabbing');
		});
	},
	calculatePos: function (event, posCurrent) {
		var _ = this;
		var imageWidth = _.imageSize.width * _.currentScale;
		var imageHeigth = _.imageSize.height * _.currentScale;

		_.mapPos.x += event.clientX - posCurrent.x;
		_.mapPos.y += event.clientY - posCurrent.y;

		_.mapPos.x = Math.min(_.mapPos.x, (imageWidth - _.windowSize.width) / 2);
		_.mapPos.y = Math.min(_.mapPos.y, (imageHeigth - _.windowSize.height) / 2);

		if (_.windowSize.width < imageWidth)
			_.mapPos.x = Math.max(_.mapPos.x, (imageWidth - _.windowSize.width) / -2);
		else
			_.mapPos.x = 0;

		if (_.windowSize.height < imageHeigth)
			_.mapPos.y = Math.max(_.mapPos.y, (imageHeigth - _.windowSize.height) / -2);
		else
			_.mapPos.y = 0;

		_.map.style.transform = 'translate(' + _.mapPos.x + 'px, ' + _.mapPos.y + 'px) scale(' + _.currentScale + ')';
	},
	zoom: function () {
		var _ = this;
		QS('body').addEventListener('wheel', _.mouseScroll);
	},
	mouseScroll: function (event) {
		var _ = WorldMap;

		if (event.wheelDelta > 0)
			_.currentScale += 0.1;
		else
			_.currentScale -= 0.1;

		_.currentScale = Math.min(_.currentScale, 2);
		_.currentScale = Math.max(_.currentScale, 0.5);

		var imageWidth = _.imageSize.width * _.currentScale;
		var imageHeigth = _.imageSize.height * _.currentScale;

		_.mapPos.x = Math.min(_.mapPos.x, (imageWidth - _.windowSize.width) / 2);
		_.mapPos.y = Math.min(_.mapPos.y, (imageHeigth - _.windowSize.height) / 2);

		if (_.windowSize.width < imageWidth)
			_.mapPos.x = Math.max(_.mapPos.x, (imageWidth - _.windowSize.width) / -2);
		else
			_.mapPos.x = 0;

		if (_.windowSize.height < imageHeigth)
			_.mapPos.y = Math.max(_.mapPos.y, (imageHeigth - _.windowSize.height) / -2);
		else
			_.mapPos.y = 0;

		_.map.style.transform = 'translate(' + _.mapPos.x + 'px, ' + _.mapPos.y + 'px) scale(' + _.currentScale + ')';
	},
	placeProbeSite: function () {
		var _ = this;
		var probesList = QS('#probes-list');
		for (var i in data.sites) {
			var probe = data.sites[i];
			if (probe.pos.y == 0)
				continue;
			_.probes[probe.site] = {
				top: Math.floor(probe.pos.y),
				left: Math.floor(probe.pos.x)
			};
			var div = document.createElementNS('http://www.w3.org/1999/xhtml', 'div');
			div.className = 'circle';
			var li = document.createElementNS('http://www.w3.org/1999/xhtml', 'li');
			li.setAttribute('data-site', probe.site);
			if (probe.site == 'NLA') {
				li.title = 'NLA';
				li.className = "nla";
			} else {
				li.title = 'FN Site ' + probe.site;
				li.tabIndex = "1";
				li.appendChild(div);
			}
			li.style.top = _.probes[probe.site].top + 'px';
			li.style.left = _.probes[probe.site].left + 'px';
			probesList.appendChild(li);
		}
	},
	addLinks: function () {
		var _ = this;
		var probesLinks = QS('#probes-links');
		var done = [];
		for (var i in data.sites) {
			var probe = data.sites[i];
			if (probe.pos.y == 0)
				continue;

			for (var j in probe.link) {
				var link = probe.link[j];
				var begin = _.probes[probe.site];
				if (!_.probes[link] || done[link + ',' + probe.site])
					continue;
				var end = _.probes[link];

				var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
				svg.setAttribute('data-link', probe.site + ',' + link);

				var height = 0;
				var width = 0;
				var p1 = {
					x: 0,
					y: 0
				};
				var p2 = {
					x: 0,
					y: 0
				};
				var pos = {
					top: 0,
					left: 0
				};

				if (end.top - begin.top > 0) { // under
					pos.top = begin.top;
					height = end.top - begin.top;
					p1.y = 0;
				} else { // over
					pos.top = end.top;
					height = begin.top - end.top;
					p1.y = height;
				}

				if (end.left - begin.left > 0) { // right
					pos.left = begin.left;
					width = end.left - begin.left;
					p1.x = 0;
				} else { // left
					pos.left = end.left;
					width = begin.left - end.left;
					p1.x = width;
				}

				if (width == 0) {
					width = 4;
					pos.left -= 1;
				}
				if (height == 0) {
					height = 4;
					pos.top -= 1;
				}

				if (end.top - begin.top <= 0)
					p1.y = height;
				if (end.left - begin.left <= 0)
					p1.x = width;

				if (p1.y == 0)
					p2.y = height;
				else
					p2.y = 0;
				if (p1.x == 0)
					p2.x = width;
				else
					p2.x = 0;

				if (width == 4)
					p1.x = p2.x = width / 2;
				if (height == 4)
					p1.y = p2.y = height / 2;

				svg.style.top = pos.top + 'px';
				svg.style.left = pos.left + 'px';
				svg.style.height = height + 'px';
				svg.style.width = width + 'px';

				var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
				// IE does not support multiple parameters for add().
				line.classList.add('booster');
				line.classList.add('white');
				line.setAttribute('x1', p1.x);
				line.setAttribute('y1', p1.y);
				line.setAttribute('x2', p2.x);
				line.setAttribute('y2', p2.y);
				svg.appendChild(line);

				var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
				line.classList.add('booster');
				line.classList.add('black');
				line.setAttribute('x1', p1.x);
				line.setAttribute('y1', p1.y);
				line.setAttribute('x2', p2.x);
				line.setAttribute('y2', p2.y);
				svg.appendChild(line);

				var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
				line.classList.add('link');
				line.classList.add('blue');
				line.setAttribute('x1', p1.x);
				line.setAttribute('y1', p1.y);
				line.setAttribute('x2', p2.x);
				line.setAttribute('y2', p2.y);
				svg.appendChild(line);

				var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
				line.classList.add('link');
				line.classList.add('white');
				line.setAttribute('x1', p1.x);
				line.setAttribute('y1', p1.y);
				line.setAttribute('x2', p2.x);
				line.setAttribute('y2', p2.y);
				svg.appendChild(line);

				probesLinks.appendChild(svg);
				done[probe.site + ',' + link] = true;
			}
		}
	}
};
