var data = {
	fn: {
		production: {
			'A': 5,
			'B': 3.5,
			'C': 2.5
		},
		revenue: {
			'A': {
				'Primordia': 8.5,
				'Noctilum': 7.5,
				'Oblivia': 7.5,
				'Sylvalum': 8.5,
				'Cauldros': 7.5
			},
			'B': 6.5,
			'C': 5.5,
			'D': 4.5,
			'E': 3,
			'F': 2
		}
	},
	probe: {
		basic: {
			type: 'basic',
			name: 'Basic Probe',
			values: {
				production: 50,
				revenue: 50
			}
		},
		pg1: {
			type: 'production',
			name: 'Mining Probe G1',
			total: 20,
			values: {
				production: 100,
				revenue: 30
			}
		},
		pg2: {
			type: 'production',
			name: 'Mining Probe G2',
			total: 24,
			values: {
				production: 120,
				revenue: 30
			}
		},
		pg3: {
			type: 'production',
			name: 'Mining Probe G3',
			total: 7,
			values: {
				production: 140,
				revenue: 30
			}
		},
		pg4: {
			type: 'production',
			name: 'Mining Probe G4',
			total: 15,
			values: {
				production: 160,
				revenue: 30
			}
		},
		pg5: {
			type: 'production',
			name: 'Mining Probe G5',
			total: 9,
			values: {
				production: 180,
				revenue: 30
			}
		},
		pg6: {
			type: 'production',
			name: 'Mining Probe G6',
			total: 10,
			values: {
				production: 200,
				revenue: 30
			}
		},
		pg7: {
			type: 'production',
			name: 'Mining Probe G7',
			total: 4,
			values: {
				production: 220,
				revenue: 30
			}
		},
		pg8: {
			type: 'production',
			name: 'Mining Probe G8',
			total: 23,
			values: {
				production: 240,
				revenue: 30
			}
		},
		pg9: {
			type: 'production',
			name: 'Mining Probe G9',
			total: 10,
			values: {
				production: 270,
				revenue: 30
			}
		},
		pg10: {
			type: 'production',
			name: 'Mining Probe G10',
			total: 4,
			values: {
				production: 300,
				revenue: 30
			}
		},
		rg1: {
			type: 'revenue',
			name: 'Research Probe G1',
			total: 3,
			values: {
				production: 30,
				revenue: 200
			}
		},
		rg2: {
			type: 'revenue',
			name: 'Research Probe G2',
			total: 4,
			values: {
				production: 30,
				revenue: 250
			}
		},
		rg3: {
			type: 'revenue',
			name: 'Research Probe G3',
			total: 2,
			values: {
				production: 30,
				revenue: 300
			}
		},
		rg4: {
			type: 'revenue',
			name: 'Research Probe G4',
			total: 6,
			values: {
				production: 30,
				revenue: 350
			}
		},
		rg5: {
			type: 'revenue',
			name: 'Research Probe G5',
			total: 7,
			values: {
				production: 30,
				revenue: 400
			}
		},
		rg6: {
			type: 'revenue',
			name: 'Research Probe G6',
			total: 4,
			values: {
				production: 30,
				revenue: 450
			}
		},
		bg1: {
			type: 'booster',
			name: 'Booster Probe G1',
			total: 3,
			values: {
				boost: 50,
				production: 10,
				revenue: 10
			}
		},
		bg2: {
			type: 'booster',
			name: 'Booster Probe G2',
			total: 3,
			values: {
				boost: 100,
				production: 10,
				revenue: 10
			}
		},
		storage: {
			type: 'storage',
			name: 'Storage Probe',
			total: 11,
			values: {
				storage: 3000,
				production: 10,
				revenue: 10
			}
		},
		duplicator: {
			type: 'duplicator',
			name: 'Duplicator Probe',
			total: 4
		},
		fuel: {
			type: 'fuel',
			name: 'Fuel Recovery Probe',
			total: 3,
			values: {
				combat: 100,
				production: 10,
				revenue: 10
			}
		},
		melee: {
			type: 'melee',
			name: 'Melee Attack Probe',
			total: 3,
			values: {
				combat: 100,
				production: 10,
				revenue: 10
			}
		},
		ranged: {
			type: 'ranged',
			name: 'Ranged Attack Probe',
			total: 3,
			values: {
				combat: 100,
				production: 10,
				revenue: 10
			}
		},
		debuff: {
			type: 'debuff',
			name: 'EZ Debuff Probe',
			total: 3,
			values: {
				combat: 100,
				production: 10,
				revenue: 10
			}
		},
		resistance: {
			type: 'resistance',
			name: 'Attribute Resistance Probe',
			total: 3,
			values: {
				combat: 100,
				production: 10,
				revenue: 10
			}
		}
	},
	sites: {
		'NLA': {
			pos: {
				x: 488,
				y: 1130
			},
			area: 'N.L.A',
			site: 'NLA',
			lvl: 0,
			link: [107]
		},
		'101': {
			pos: {
				x: 322,
				y: 912
			},
			area: 'Primordia',
			site: 101,
			lvl: 5,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'S'
			},
			spot: 1,
			ressources: ['None'],
			link: [105]
		},
		'102': {
			pos: {
				x: 322,
				y: 1130
			},
			area: 'Primordia',
			site: 102,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [104]
		},
		'103': {
			pos: {
				x: 322,
				y: 986
			},
			area: 'Primordia',
			site: 103,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'A'
			},
			spot: 1,
			ressources: ['None'],
			link: [105, 106, 222]
		},
		'104': {
			pos: {
				x: 350,
				y: 1076
			},
			area: 'Primordia',
			site: 104,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [102, 106]
		},
		'105': {
			pos: {
				x: 376,
				y: 912
			},
			area: 'Primordia',
			site: 105,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [101, 103, 109]
		},
		'106': {
			pos: {
				x: 376,
				y: 1022
			},
			area: 'Primordia',
			site: 106,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'E',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [103, 104, 107]
		},
		'107': {
			pos: {
				x: 404,
				y: 1112
			},
			area: 'Primordia',
			site: 107,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: ['NLA', 106, 110]
		},
		'108': {
			pos: {
				x: 432,
				y: 986
			},
			area: 'Primordia',
			site: 108,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Arc Sand Ore, Foucaultium'],
			link: [109]
		},
		'109': {
			pos: {
				x: 460,
				y: 930
			},
			area: 'Primordia',
			site: 109,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Foucaultium', 'Dawnstone, Lionbone Bort'],
			link: [105, 108]
		},
		'110': {
			pos: {
				x: 460,
				y: 1040
			},
			area: 'Primordia',
			site: 110,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Aurorite', 'Arc Sand Ore, White Cometite', 'Dawnstone'],
			link: [107, 111, 112]
		},
		'111': {
			pos: {
				x: 488,
				y: 950
			},
			area: 'Primordia',
			site: 111,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Foucaultium'],
			link: [110, 113]
		},
		'112': {
			pos: {
				x: 514,
				y: 1040
			},
			area: 'Primordia',
			site: 112,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'F',
				combat: 'A'
			},
			spot: 0,
			ressources: ['None'],
			link: [110, 114, 115]
		},
		'113': {
			pos: {
				x: 542,
				y: 876
			},
			area: 'Primordia',
			site: 113,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [111, 409]
		},
		'114': {
			pos: {
				x: 542,
				y: 1096
			},
			area: 'Primordia',
			site: 114,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [112, 116]
		},
		'115': {
			pos: {
				x: 570,
				y: 1002
			},
			area: 'Primordia',
			site: 115,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Arc Sand Ore', 'White Cometite, Lionbone Bort'],
			link: [112]
		},
		'116': {
			pos: {
				x: 596,
				y: 1130
			},
			area: 'Primordia',
			site: 116,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [114, 117]
		},
		'117': {
			pos: {
				x: 624,
				y: 1076
			},
			area: 'Primordia',
			site: 117,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'D',
				combat: 'A'
			},
			spot: 1,
			ressources: ['None'],
			link: [116, 118, 120]
		},
		'118': {
			pos: {
				x: 680,
				y: 1040
			},
			area: 'Primordia',
			site: 118,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'White Cometite, Lionbone Bort'],
			link: [117, 121]
		},
		'119': {
			pos: {
				x: 680,
				y: 1150
			},
			area: 'Primordia',
			site: 119,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [120]
		},
		'120': {
			pos: {
				x: 706,
				y: 1096
			},
			area: 'Primordia',
			site: 120,
			lvl: 3,
			rankName: {
				production: 'B',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [117, 119]
		},
		'121': {
			pos: {
				x: 734,
				y: 1040
			},
			area: 'Primordia',
			site: 121,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [118, 301]
		},
		'201': {
			pos: {
				x: 69,
				y: 423
			},
			area: 'Noctilum',
			site: 201,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [206]
		},
		'202': {
			pos: {
				x: 69,
				y: 530
			},
			area: 'Noctilum',
			site: 202,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Cimmerian Cinnabar', 'Everfreeze Ore'],
			link: [203, 207, 208]
		},
		'203': {
			pos: {
				x: 69,
				y: 602
			},
			area: 'Noctilum',
			site: 203,
			lvl: 0,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Cimmerian Cinnabar'],
			link: [202, 204]
		},
		'204': {
			pos: {
				x: 96,
				y: 652
			},
			area: 'Noctilum',
			site: 204,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [203, 205, 211, 212]
		},
		'205': {
			pos: {
				x: 96,
				y: 724
			},
			area: 'Noctilum',
			site: 205,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [204, 209]
		},
		'206': {
			pos: {
				x: 122,
				y: 388
			},
			area: 'Noctilum',
			site: 206,
			lvl: 5,
			rankName: {
				production: 'B',
				revenue: 'A',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [201, 207, 213]
		},
		'207': {
			pos: {
				x: 122,
				y: 494
			},
			area: 'Noctilum',
			site: 207,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Infernium', 'White Cometite, Cimmerian Cinnabar', 'Foucaultium'],
			link: [202, 206]
		},
		'208': {
			pos: {
				x: 122,
				y: 566
			},
			area: 'Noctilum',
			site: 208,
			lvl: 2,
			rankName: {
				production: 'B',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Foucaultium'],
			link: [202]
		},
		'209': {
			pos: {
				x: 122,
				y: 778
			},
			area: 'Noctilum',
			site: 209,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [205]
		},
		'210': {
			pos: {
				x: 148,
				y: 548
			},
			area: 'Noctilum',
			site: 210,
			lvl: 0,
			rankName: {
				production: 'B',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [211]
		},
		'211': {
			pos: {
				x: 148,
				y: 618
			},
			area: 'Noctilum',
			site: 211,
			lvl: 2,
			rankName: {
				production: 'A',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [204, 210]
		},
		'212': {
			pos: {
				x: 148,
				y: 690
			},
			area: 'Noctilum',
			site: 212,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Enduron Lead, White Cometite'],
			link: [204, 216]
		},
		'213': {
			pos: {
				x: 174,
				y: 423
			},
			area: 'Noctilum',
			site: 213,
			lvl: 4,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			rankValue: {
				revenue: 8.5
			},
			spot: 1,
			ressources: ['None'],
			link: [206]
		},
		'214': {
			pos: {
				x: 174,
				y: 566
			},
			area: 'Noctilum',
			site: 214,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 2,
			ressources: ['None'],
			link: [215]
		},
		'215': {
			pos: {
				x: 174,
				y: 636
			},
			area: 'Noctilum',
			site: 215,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Enduron Lead, Everfreeze Ore', 'Foucaultium'],
			link: [214, 218]
		},
		'216': {
			pos: {
				x: 174,
				y: 742
			},
			area: 'Noctilum',
			site: 216,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'A'
			},
			spot: 1,
			ressources: ['None'],
			link: [212, 218, 225]
		},
		'217': {
			pos: {
				x: 174,
				y: 920
			},
			area: 'Noctilum',
			site: 217,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Infernium, Cimmerian Cinnabar'],
			link: [222]
		},
		'218': {
			pos: {
				x: 200,
				y: 690
			},
			area: 'Noctilum',
			site: 218,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Enduron Lead, White Cometite'],
			link: [215, 216, 224]
		},
		'219': {
			pos: {
				x: 200,
				y: 866
			},
			area: 'Noctilum',
			site: 219,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Enduron Lead', 'White Cometite'],
			link: [220]
		},
		'220': {
			pos: {
				x: 226,
				y: 812
			},
			area: 'Noctilum',
			site: 220,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'A'
			},
			spot: 1,
			ressources: ['Infernium', 'Everfreeze Ore'],
			link: [219, 221, 225]
		},
		'221': {
			pos: {
				x: 226,
				y: 884
			},
			area: 'Noctilum',
			site: 221,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 2,
			ressources: ['None'],
			link: [220, 222]
		},
		'222': {
			pos: {
				x: 226,
				y: 954
			},
			area: 'Noctilum',
			site: 222,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [103, 217, 221]
		},
		'223': {
			pos: {
				x: 252,
				y: 618
			},
			area: 'Noctilum',
			site: 223,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'F',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [224]
		},
		'224': {
			pos: {
				x: 252,
				y: 652
			},
			area: 'Noctilum',
			site: 224,
			lvl: 4,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [218, 223]
		},
		'225': {
			pos: {
				x: 252,
				y: 760
			},
			area: 'Noctilum',
			site: 225,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [216, 220]
		},
		'301': {
			pos: {
				x: 770,
				y: 1060
			},
			area: 'Oblivia',
			site: 301,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Infernium', 'Arc Sand Ore, Lionbone Bort'],
			link: [121, 302, 303]
		},
		'302': {
			pos: {
				x: 826,
				y: 1024
			},
			area: 'Oblivia',
			site: 302,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [301]
		},
		'303': {
			pos: {
				x: 826,
				y: 1134
			},
			area: 'Oblivia',
			site: 303,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'White Cometite'],
			link: [301, 306]
		},
		'304': {
			pos: {
				x: 854,
				y: 1080
			},
			area: 'Oblivia',
			site: 304,
			lvl: 5,
			rankName: {
				production: 'B',
				revenue: 'A',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [305, 306, 309]
		},
		'305': {
			pos: {
				x: 882,
				y: 986
			},
			area: 'Oblivia',
			site: 305,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Arc Sand Ore, Enduron Ore'],
			link: [304, 308]
		},
		'306': {
			pos: {
				x: 908,
				y: 1116
			},
			area: 'Oblivia',
			site: 306,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [303, 304, 307]
		},
		'307': {
			pos: {
				x: 908,
				y: 1188
			},
			area: 'Oblivia',
			site: 307,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Infernium', 'Arc Sand Ore', 'Enduron Lead', 'White Cometite'],
			link: [306, 313]
		},
		'308': {
			pos: {
				x: 936,
				y: 914
			},
			area: 'Oblivia',
			site: 308,
			lvl: 2,
			rankName: {
				production: 'B',
				revenue: 'C',
				combat: 'A'
			},
			spot: 0,
			ressources: ['Ouroboros Crystal'],
			link: [305]
		},
		'309': {
			pos: {
				x: 936,
				y: 1024
			},
			area: 'Oblivia',
			site: 309,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Enduron Lead', 'Ouroboros Crystal'],
			link: [304, 311]
		},
		'310': {
			pos: {
				x: 992,
				y: 914
			},
			area: 'Oblivia',
			site: 310,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [311]
		},
		'311': {
			pos: {
				x: 992,
				y: 986
			},
			area: 'Oblivia',
			site: 311,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [309, 310]
		},
		'312': {
			pos: {
				x: 992,
				y: 1096
			},
			area: 'Oblivia',
			site: 312,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Infernium', 'Boiled-Egg Ore', 'Lionbone Bort'],
			link: [313, 315]
		},
		'313': {
			pos: {
				x: 992,
				y: 1170
			},
			area: 'Oblivia',
			site: 313,
			lvl: 1,
			rankName: {
				production: 'C',
				revenue: 'E',
				combat: 'A'
			},
			spot: 2,
			ressources: ['None'],
			link: [307, 312, 314]
		},
		'314': {
			pos: {
				x: 992,
				y: 1244
			},
			area: 'Oblivia',
			site: 314,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [313]
		},
		'315': {
			pos: {
				x: 1018,
				y: 1006
			},
			area: 'Oblivia',
			site: 315,
			lvl: 3,
			rankName: {
				production: 'A',
				revenue: 'A',
				combat: 'B'
			},
			rankValue: {
				revenue: 8.5
			},
			spot: 2,
			ressources: ['None'],
			link: [312, 316, 318, 321]
		},
		'316': {
			pos: {
				x: 1046,
				y: 914
			},
			area: 'Oblivia',
			site: 316,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [315]
		},
		'317': {
			pos: {
				x: 1046,
				y: 1170
			},
			area: 'Oblivia',
			site: 317,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [318, 319]
		},
		'318': {
			pos: {
				x: 1072,
				y: 1080
			},
			area: 'Oblivia',
			site: 318,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 2,
			ressources: ['Boiled-Egg Ore', 'White Cometite', 'Lionbone Bort'],
			link: [315, 317]
		},
		'319': {
			pos: {
				x: 1072,
				y: 1188
			},
			area: 'Oblivia',
			site: 319,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Infernium', 'Boiled-Egg Ore'],
			link: [317]
		},
		'320': {
			pos: {
				x: 1102,
				y: 914
			},
			area: 'Oblivia',
			site: 320,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Aurorite', 'Ouroboros Crystal'],
			link: [321]
		},
		'321': {
			pos: {
				x: 1102,
				y: 986
			},
			area: 'Oblivia',
			site: 321,
			lvl: 2,
			rankName: {
				production: 'A',
				revenue: 'D',
				combat: 'A'
			},
			spot: 0,
			ressources: ['None'],
			link: [315, 320, 322]
		},
		'322': {
			pos: {
				x: 1156,
				y: 1060
			},
			area: 'Oblivia',
			site: 322,
			lvl: 4,
			rankName: {
				production: 'A',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [321]
		},
		'401': {
			pos: {
				x: 618,
				y: 494
			},
			area: 'Sylvalum',
			site: 401,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Parhelion Platinum'],
			link: [402, 404]
		},
		'402': {
			pos: {
				x: 646,
				y: 548
			},
			area: 'Sylvalum',
			site: 402,
			lvl: 3,
			rankName: {
				production: 'A',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [401, 408]
		},
		'403': {
			pos: {
				x: 646,
				y: 658
			},
			area: 'Sylvalum',
			site: 403,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'C',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [405]
		},
		'404': {
			pos: {
				x: 674,
				y: 418
			},
			area: 'Sylvalum',
			site: 404,
			lvl: 5,
			rankName: {
				production: 'B',
				revenue: 'A',
				combat: 'S'
			},
			spot: 1,
			ressources: ['None'],
			link: [401, 407]
		},
		'405': {
			pos: {
				x: 674,
				y: 602
			},
			area: 'Sylvalum',
			site: 405,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'E',
				combat: 'A'
			},
			spot: 0,
			ressources: ['Arc Sand Ore'],
			link: [403, 408, 409]
		},
		'406': {
			pos: {
				x: 700,
				y: 474
			},
			area: 'Sylvalum',
			site: 406,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [408]
		},
		'407': {
			pos: {
				x: 730,
				y: 382
			},
			area: 'Sylvalum',
			site: 407,
			lvl: 3,
			rankName: {
				production: 'A',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [404, 412]
		},
		'408': {
			pos: {
				x: 730,
				y: 530
			},
			area: 'Sylvalum',
			site: 408,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'D',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Aurorite', 'Arc Sand Ore, Everfreeze Ore'],
			link: [402, 405, 406, 413]
		},
		'409': {
			pos: {
				x: 730,
				y: 678
			},
			area: 'Sylvalum',
			site: 409,
			lvl: 3,
			rankName: {
				production: 'B',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [113, 405, 411]
		},
		'410': {
			pos: {
				x: 756,
				y: 326
			},
			area: 'Sylvalum',
			site: 410,
			lvl: 4,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [412]
		},
		'411': {
			pos: {
				x: 756,
				y: 584
			},
			area: 'Sylvalum',
			site: 411,
			lvl: 4,
			rankName: {
				production: 'A',
				revenue: 'A',
				combat: 'S'
			},
			rankValue: {
				revenue: 7.5
			},
			spot: 0,
			ressources: ['None'],
			link: [409, 414]
		},
		'412': {
			pos: {
				x: 782,
				y: 418
			},
			area: 'Sylvalum',
			site: 412,
			lvl: 3,
			rankName: {
				production: 'A',
				revenue: 'B',
				combat: 'A'
			},
			spot: 0,
			ressources: ['None'],
			link: [407, 410, 415]
		},
		'413': {
			pos: {
				x: 782,
				y: 494
			},
			area: 'Sylvalum',
			site: 413,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			rankValue: {
				revenue: 7.5
			},
			spot: 1,
			ressources: ['None'],
			link: [408, 416]
		},
		'414': {
			pos: {
				x: 810,
				y: 620
			},
			area: 'Sylvalum',
			site: 414,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 2,
			ressources: ['Parhelion Platinum', 'Marine rutile'],
			link: [411]
		},
		'415': {
			pos: {
				x: 838,
				y: 344
			},
			area: 'Sylvalum',
			site: 415,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [412, 502]
		},
		'416': {
			pos: {
				x: 838,
				y: 454
			},
			area: 'Sylvalum',
			site: 416,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [413, 418, 419]
		},
		'417': {
			pos: {
				x: 838,
				y: 564
			},
			area: 'Sylvalum',
			site: 417,
			lvl: 0,
			rankName: {
				production: 'B',
				revenue: 'D',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Everfreeze Ore', 'Boiled-Egg Ore'],
			link: [419]
		},
		'418': {
			pos: {
				x: 866,
				y: 400
			},
			area: 'Sylvalum',
			site: 418,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			rankValue: {
				revenue: 5.5
			},
			spot: 0,
			ressources: ['Parhelion Platinum', 'Arc Sand Ore', 'Everfreeze Ore', 'Boiled-Egg Ore', 'Marine Rutile'],
			link: [416]
		},
		'419': {
			pos: {
				x: 866,
				y: 510
			},
			area: 'Sylvalum',
			site: 419,
			lvl: 5,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'S'
			},
			spot: 1,
			ressources: ['None'],
			link: [416, 417, 420]
		},
		'420': {
			pos: {
				x: 920,
				y: 474
			},
			area: 'Sylvalum',
			site: 420,
			lvl: 2,
			rankName: {
				production: 'B',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [419]
		},
		'501': {
			pos: {
				x: 806,
				y: 128
			},
			area: 'Cauldros',
			site: 501,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'F',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Arc sand Ore'],
			link: [502]
		},
		'502': {
			pos: {
				x: 806,
				y: 244
			},
			area: 'Cauldros',
			site: 502,
			lvl: 1,
			rankName: {
				production: 'A',
				revenue: 'C',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Bonjelium'],
			link: [415, 501, 503]
		},
		'503': {
			pos: {
				x: 862,
				y: 166
			},
			area: 'Cauldros',
			site: 503,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'D',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Enduron Lead'],
			link: [502, 504]
		},
		'504': {
			pos: {
				x: 892,
				y: 262
			},
			area: 'Cauldros',
			site: 504,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'C',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Arc Sand Ore', 'Enduron Lead, Marine Rutile', 'Bonjelium'],
			link: [503, 508]
		},
		'505': {
			pos: {
				x: 920,
				y: 128
			},
			area: 'Cauldros',
			site: 505,
			lvl: 2,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 2,
			ressources: ['None'],
			link: [506, 509]
		},
		'506': {
			pos: {
				x: 920,
				y: 204
			},
			area: 'Cauldros',
			site: 506,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Bonjelium', 'Arc Sand Ore'],
			link: [505]
		},
		'507': {
			pos: {
				x: 948,
				y: 338
			},
			area: 'Cauldros',
			site: 507,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['Bonjelium'],
			link: [508]
		},
		'508': {
			pos: {
				x: 978,
				y: 244
			},
			area: 'Cauldros',
			site: 508,
			lvl: 2,
			rankName: {
				production: 'A',
				revenue: 'B',
				combat: 'S'
			},
			spot: 1,
			ressources: ['Enduron Lead', 'Marine Rutile'],
			link: [504, 507, 509, 511]
		},
		'509': {
			pos: {
				x: 1006,
				y: 146
			},
			area: 'Cauldros',
			site: 509,
			lvl: 2,
			rankName: {
				production: 'A',
				revenue: 'A',
				combat: 'A'
			},
			spot: 0,
			ressources: ['None'],
			link: [505, 508, 510, 513]
		},
		'510': {
			pos: {
				x: 1034,
				y: 50
			},
			area: 'Cauldros',
			site: 510,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'B'
			},
			spot: 0,
			ressources: ['Bonjelium'],
			link: [509]
		},
		'511': {
			pos: {
				x: 1034,
				y: 244
			},
			area: 'Cauldros',
			site: 511,
			lvl: 2,
			rankName: {
				production: 'A',
				revenue: 'C',
				combat: 'A'
			},
			spot: 0,
			ressources: ['Bonjelium'],
			link: [508, 512, 514]
		},
		'512': {
			pos: {
				x: 1034,
				y: 320
			},
			area: 'Cauldros',
			site: 512,
			lvl: 4,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [511]
		},
		'513': {
			pos: {
				x: 1064,
				y: 146
			},
			area: 'Cauldros',
			site: 513,
			lvl: 4,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 2,
			ressources: ['None'],
			link: [509, 516]
		},
		'514': {
			pos: {
				x: 1092,
				y: 282
			},
			area: 'Cauldros',
			site: 514,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'A',
				combat: 'B'
			},
			spot: 1,
			ressources: ['None'],
			link: [511, 515]
		},
		'515': {
			pos: {
				x: 1120,
				y: 224
			},
			area: 'Cauldros',
			site: 515,
			lvl: 3,
			rankName: {
				production: 'C',
				revenue: 'B',
				combat: 'S'
			},
			spot: 0,
			ressources: ['None'],
			link: [514]
		},
		'516': {
			pos: {
				x: 1150,
				y: 128
			},
			area: 'Cauldros',
			site: 516,
			lvl: 1,
			rankName: {
				production: 'B',
				revenue: 'E',
				combat: 'B'
			},
			spot: 0,
			ressources: ['None'],
			link: [513]
		}
	}
};
