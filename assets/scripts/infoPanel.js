var InfoPanel = {
	fnInfo: null,
	probeInfo: null,
	probeCount: {},
	currentSite: null,
	init: function () {
		var _ = this;
		_.fnInfo = QS('#fn-info');
		_.probeInfo = QS('#probe-info');
		_.buildTypeList();
		_.bind();
	},
	buildSpotList: function (site) {
		var _ = this;
		var select = QS('.site-spot', _.fnInfo);

		var options = QSA('option', select);
		for (var o = 0; o < options.length; o++)
			select.removeChild(options[o]);

		var option = null;
		for (var i = 0; i <= data.sites[site].spot; i++) {
			option = document.createElementNS('http://www.w3.org/1999/xhtml', 'option');
			option.value = i;
			option.innerText = i;
			select.appendChild(option);
		}
	},
	buildTypeList: function (site) {
		var _ = this;
		var select = QS('.probe-type', _.probeInfo);
		var probe = null;
		var option = null;
		for (var i in data.probe) {
			probe = data.probe[i];

			option = document.createElementNS('http://www.w3.org/1999/xhtml', 'option');
			option.value = i;
			option.innerHTML = probe.name.padRight('&nbsp;', 27);

			if (typeof probe.total == 'number' && !Main.IEfix) {
				var count = document.createElementNS('http://www.w3.org/1999/xhtml', 'span');
				count.innerHTML = (typeof probe.count == 'number') ? probe.count : '&nbsp;' + 0;
				count.id = 'count-' + i;
				_.probeCount[i] = count;
				option.appendChild(count);

				var total = document.createElementNS('http://www.w3.org/1999/xhtml', 'span');
				total.innerHTML += ' (' + probe.total + ')';
				option.appendChild(total);
			}

			select.appendChild(option);
		}
	},
	bind: function () {
		var _ = this;
		var selectSpot = QS('.site-spot', _.fnInfo);
		selectSpot.addEventListener('change', function () {
			var d = Probes.getProbe(_.currentSite);
			Probes.setProbeSpot(d.site, this.value);
			Probes.updateLinked();
			Probes.setLocal();
			Ressources.update();
			Share.getLink();
			_.displayProbeData(d.site);
		});
		var selectType = QS('.probe-type', _.probeInfo);
		selectType.addEventListener('change', function () {
			var d = Probes.getProbe(_.currentSite);
			Probes.setProbe(d.site, this.value);
			Probes.updateLinked();
			Probes.setLocal();
			Ressources.update();
			Share.getLink();
			_.displayProbeData(d.site);
		});
	},
	displayProbeData: function (site) {
		var _ = this;
		var probeData = Probes.getProbe(site);
		var probeRessources = {};
		if (probeData.current.type == 'duplicator') {
			var linkedSites = Probes.getLinkedSites(site);
			var linkedSite = null;
			var tempStats = {
				storage: 0,
				production: 0,
				revenue: 0
			};
			for (var l = 0; l < linkedSites.length; l++) {
				linkedSite = linkedSites[l];
				var temp = Ressources.getProbeStats(Probes.getProbe(linkedSite), site, probeData);

				tempStats.production += temp.production;
				tempStats.revenue += temp.revenue;
				tempStats.storage += temp.storage;
			}
			probeRessources = tempStats;
		} else {
			probeRessources = Ressources.getProbeStats(probeData, site, false);
		}
		_.currentSite = site;
		QS('.site-name', _.fnInfo).innerText = probeData.site;
		QS('.site-production-rank', _.fnInfo).innerText = probeData.rankName.production;
		QS('.site-revenue-rank', _.fnInfo).innerText = probeData.rankName.revenue;
		QS('.site-combat-rank', _.fnInfo).innerText = probeData.rankName.combat;
		QS('.site-spot-rank', _.fnInfo).innerText = probeData.spot;
		QS('.site-ressources', _.fnInfo).innerText = probeData.ressources.join(', ');

		QS('.site-production', _.fnInfo).innerText = probeRessources.production;
		QS('.site-revenue', _.fnInfo).innerText = probeRessources.revenue;
		QS('.site-storage', _.fnInfo).innerText = probeRessources.storage;

		if (probeData.current.spot)
			QS('.site-spot', _.fnInfo).value = probeData.current.spot;
		_.fnInfo.className = 'display';

		QS('.production', _.probeInfo).style.display = 'inline';
		QS('.revenue', _.probeInfo).style.display = 'inline';
		QS('.storage', _.probeInfo).style.display = 'inline';
		QS('.bonus', _.probeInfo).style.display = 'inline';
		QS('.combat', _.probeInfo).style.display = 'none';
		QS('.boost', _.probeInfo).style.display = 'none';
		QS('.storage', _.probeInfo).style.display = 'none';

		QS('.probe-type', _.probeInfo).value = probeData.current.id;
		if (probeData.current.type == 'storage') {
			QS('.storage', _.probeInfo).style.display = 'inline';
			QS('.probe-storage-stats', _.probeInfo).innerText = probeData.current.values.storage;
		}

		if (probeData.current.type == 'booster') {
			QS('.bonus', _.probeInfo).style.display = 'none';
			QS('.boost', _.probeInfo).style.display = 'inline';
			QS('.probe-boost', _.probeInfo).innerText = data.probe[probeData.current.id].values.boost + '%';
		}

		if (['fuel', 'melee', 'ranged', 'debuff', 'resistance'].inArray(probeData.current.type)) {
			QS('.bonus', _.probeInfo).style.display = 'none';
			QS('.combat', _.probeInfo).style.display = 'inline';
			QS('.probe-combat-stats', _.probeInfo).innerText = data.probe[probeData.current.id].values.combat + '%';
		}
		var bonus = Probes.getBonus(probeData);
		if (bonus > 1)
			bonus = Math.floor(((bonus - 1) * 100)) + '%';
		else
			bonus = 'None';

		if (probeData.current.type == 'duplicator') {
			QS('.production', _.probeInfo).style.display = 'none';
			QS('.revenue', _.probeInfo).style.display = 'none';
			QS('.storage', _.probeInfo).style.display = 'none';
			QS('.bonus', _.probeInfo).style.display = 'none';
		} else {
			QS('.probe-bonus', _.probeInfo).innerText = bonus;
			QS('.probe-production-stats', _.probeInfo).innerText = probeData.current.values.production;
			QS('.probe-revenue-stats', _.probeInfo).innerText = probeData.current.values.revenue;
		}
		_.probeInfo.className = 'display';
	},
	closeProbeData: function () {
		var _ = this;
		_.fnInfo.className = _.fnInfo.className.replace(/\s*display/, '');
		_.probeInfo.className = _.probeInfo.className.replace(/\s*display/, '');
	}
};
