if (!Array.prototype.inArray) {
	Array.prototype.inArray = function (needle) {
		for (var i = 0; i < this.length; i++) {
			if (this[i] == needle)
				return true;
		}
		return false;
	};
}
if (!Array.prototype.remove) {
	Array.prototype.remove = function () {
		var what,
		a = arguments,
		L = a.length,
		ax;
		while (L && this.length) {
			what = a[--L];
			while ((ax = this.indexOf(what)) !== -1)
				this.splice(ax, 1);
		}
		return this;
	};
}
if (!String.prototype.padLeft) {
	String.prototype.padLeft = function (chr, length) {
		var fill = Array(Math.max(0, length - this.length)).fill(chr);
		return fill.join('') + this;
	};
}
if (!String.prototype.padRight) {
	String.prototype.padRight = function (chr, length) {
		var fill = Array(Math.max(0, length - this.length)).fill(chr);
		return this + fill.join('');
	};
}
if (!Array.prototype.fill) {
	Array.prototype.fill = function (value) {
		var O = Object(this);
		var len = O.length >>> 0;
		var start = arguments[1];
		var relativeStart = start >> 0;
		var k = relativeStart < 0 ? Math.max(len + relativeStart, 0) : Math.min(relativeStart, len);
		var end = arguments[2];
		var relativeEnd = end === undefined ? len : end >> 0;
		var final = relativeEnd < 0 ? Math.max(len + relativeEnd, 0) : Math.min(relativeEnd, len);
		for (; k < final; k++)
			O[k] = value;
		return O;
	};
}
function QS(selector, object = null) {
	var o = document;
	if (typeof object == 'object' && object)
		o = object;
	return o.querySelector(selector);
};

function QSA(selector, object = null) {
	var o = document;
	if (typeof object == 'object' && object)
		o = object;
	return o.querySelectorAll(selector);
};

window.onload = function () {
	Main.init();
};
