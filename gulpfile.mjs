import gulp from 'gulp';
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import rename from "gulp-rename";
import sass from "gulp-dart-sass";
import gulpSourcemaps from "gulp-sourcemaps";
import gulpUglifyes from "gulp-uglify-es";
import babel from "gulp-babel";
import { obj as through2 } from "through2";
const { task, src, dest, parallel, watch, series } = gulp;
const { init, write } = gulpSourcemaps;
var uglifyes = gulpUglifyes.default;

var paths = {
    // DEV
    // SASS
    assetsSASS: "./assets/sass/",
    // JS
    assetsJS: "./assets/scripts/",

    // Compiled assets
    rootAssets: "./public/",
};

const updateTimestamp = function () {
    return through2((chunk, enc, cb) => {
        let date = new Date();
        chunk.stat.atime = date;
        chunk.stat.mtime = date;
        cb(null, chunk);
    });
};

task("vendor-js", function () {
    return src([paths.assetsJS + "vendor/*.js"])
        .pipe(concat("vendor.min.js"))
        // .pipe(babel({ presets: ["@babel/preset-env"], }))
        .pipe(uglifyes({
            // compress: false,
        }))
        .pipe(dest(paths.rootAssets + "js/vendor/"));
});


/*****************************/
/* Task for WATCH or INSTALL */
/*****************************/

// assets sass (force compass css preprocessor) to root css (styles.css & map)
task("gulp-sass", function () {
    return src(paths.assetsSASS + "style.scss")
        .pipe(init())
        .pipe(sass({ compass: true }))
        .pipe(cleanCSS({ compatibility: "ie9", level: 2, debug: true }, details => {
            console.log(`originalSize: ${details.name}: ${details.stats.originalSize}`);
            console.log(`minifiedSize: ${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(rename({ suffix: ".min" }))
        .pipe(write("."))
        .pipe(updateTimestamp())
        .pipe(dest(paths.rootAssets + "css/"));
});


// minify js main & section for dev version
task("task-js", function () {
    return src(paths.assetsJS + "*.js")
        .pipe(init())
        .pipe(concat("scripts.min.js"))
        .pipe(babel({ presets: ["@babel/preset-env"], }))
        .pipe(uglifyes({ compress: false, }))
        .pipe(write("."))
        .pipe(dest(paths.rootAssets + "js/"));
});


/********/
/* Task */
/********/
//task by default (gulp) Build css, scss
task("default", series(["gulp-sass", "task-js"]));

task("install", series(["vendor-js", "gulp-sass", "task-js"]));

task("vendor", series(["vendor-js"]));

task("vendor-all", series(["vendor"]));


/*********/
/* Watch */
/*********/
task("dev", parallel(function () {
    //assets SCSS to root CSS minify styles.min.css
    watch(paths.assetsSASS + "*.scss", { usePolling: true }, series(["gulp-sass"]));

    //assets section JS to root section JS
    watch(paths.assetsJS + "*.js", { usePolling: true }, series(["task-js"]));
    watch(paths.assetsJS + "vendor/*.js", { usePolling: true }, series(["vendor-js"]));
}));
